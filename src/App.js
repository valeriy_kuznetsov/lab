import React, { memo } from 'react';

import Main from './components/Main';

import './App.css';

function App() {
  return (
    <div className="app">
      <header className="app-header">
        Система ведения клиентов
      </header>
      <Main />
    </div>
  );
}

export default memo(App);
