import React, { memo, useState, useCallback } from "react";
import cn from "classnames";

import "./index.css";

const Documents = ({ addPerson, deletePerson, persons }) => {
  const [personForEdit, setPersonForEdit] = useState({});
  const [selectedId, setSelectedId] = useState(null);

  const [inn, setInn] = useState("");
  const [type, setType] = useState("Физ. лицо");

  const onEdit = () => {
    const targetPerson = persons.find((item) => item.id === selectedId);

    setType(targetPerson.type);
    setInn(targetPerson.inn);
    setPersonForEdit(targetPerson);
  };

  const onSelect = useCallback(
    (id) => {
      setSelectedId(selectedId && selectedId === id ? null : id);
      setPersonForEdit({});
      setInn("");
      setType("Паспорт");
    },
    [selectedId]
  );

  const onDel = useCallback(() => {
    deletePerson(selectedId);
    setSelectedId(null);
  }, [deletePerson, selectedId]);

  const handleChange = ({ target }) => {
    const { name, value } = target;

    if (name === "inn") setInn(value.trim());
    if (name === "type") setType(value);
  };

  const onAdd = useCallback(
    (e) => {
      e.preventDefault();

      if (inn.trim() !== "") {
        const data = {
          ...personForEdit,
          inn,
          type,
        };
        setInn("");
        setType("Паспорт");
        setPersonForEdit({});
        addPerson(data);
      }
    },
    [addPerson, inn, personForEdit, type]
  );

  return (
    <div className="persons">
      <div className="buttons">
        <button onClick={onEdit} disabled={!selectedId}>
          Редактировать
        </button>
        <button onClick={onDel} disabled={!selectedId || personForEdit.id}>
          Удалить
        </button>
      </div>
      <table className="persons-list">
        <thead>
          <tr className="table-header">
            <th>id</th>
            <th>Шифр клиента</th>
            <th>ИНН</th>
            <th>Тип</th>
            <th>Дата регистрации клиента</th>
          </tr>
        </thead>
        <tbody>
          {persons.map((item) => (
            <tr
              key={item.id}
              onClick={() => onSelect(item.id)}
              className={cn("list-element", {
                "list-element--active": item.id === selectedId,
              })}
            >
              <td>{item.id}</td>
              <td>{item.shifer}</td>
              <td>{item.inn}</td>
              <td>{item.type}</td>
              <td>{item.date}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <form onSubmit={onAdd} className="add-form">
        <input
          type="number"
          value={inn}
          onChange={handleChange}
          name="inn"
          placeholder="ИНН"
        />
        <select name="type" value={type} onChange={handleChange}>
          <option value="Физ. лицо">Физ. лицо</option>
          <option value="Юр. лицо">Юр. лицо</option>
        </select>
        <button disabled={!inn || inn.length < 6} type="submit">
          {personForEdit ? "Сохранить" : "Добавить"}
        </button>
      </form>
    </div>
  );
};

export default memo(Documents);
