import React, { memo, useState, useCallback } from "react";
import cn from "classnames";

import "./index.css";

const Sitizens = ({
  addCitizen,
  deleteCitizen,
  citizens,
  persons,
  documents,
}) => {
  const [citizenForEdit, setCitizenForEdit] = useState({});
  const [selectedId, setSelectedId] = useState(null);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [secondName, setSecondName] = useState("");
  const [personId, setPersonId] = useState("");
  const [documentId, setDocumentId] = useState("");

  const onEdit = () => {
    const targetCitizen = citizens.find((item) => item.id === selectedId);

    setFirstName(targetCitizen.firstName);
    setLastName(targetCitizen.lastName);
    setSecondName(targetCitizen.secondName);
    setPersonId(targetCitizen.personId);
    setDocumentId(targetCitizen.documentId);
    setCitizenForEdit(targetCitizen);
  };

  const onDel = useCallback(() => {
    deleteCitizen(selectedId);
    setSelectedId(null);
  }, [deleteCitizen, selectedId]);

  const handleChange = ({ target }) => {
    const { name, value } = target;

    if (name === "firstName") setFirstName(value.trim());
    if (name === "secondName") setSecondName(value.trim());
    if (name === "lastName") setLastName(value.trim());
    if (name === "personId") setPersonId(value);
    if (name === "documentId") setDocumentId(value);
  };

  const onSelect = useCallback(
    (id) => {
      setSelectedId(selectedId && selectedId === id ? null : id);
      setFirstName("");
      setSecondName("");
      setLastName("");
      setPersonId("");
      setDocumentId("");
      setCitizenForEdit({});
    },
    [selectedId]
  );

  const dataValidate = useCallback(() => {
    if (firstName === "") return false;
    if (secondName === "") return false;
    if (lastName === "") return false;
    if (personId === "") return false;
    if (documentId === "") return false;

    return true;
  }, [documentId, firstName, lastName, personId, secondName]);

  const onAdd = useCallback(
    (e) => {
      e.preventDefault();

      if (dataValidate()) {
        const data = {
          ...citizenForEdit,
          firstName,
          lastName,
          secondName,
          personId,
          documentId,
        };
        console.log("here_data", data);
        setFirstName("");
        setSecondName("");
        setLastName("");
        setPersonId("");
        setDocumentId("");
        setCitizenForEdit({});
        addCitizen(data);
      }
    },
    [
      addCitizen,
      citizenForEdit,
      dataValidate,
      documentId,
      firstName,
      lastName,
      personId,
      secondName,
    ]
  );

  const getProperty = (id, list, prop) => {
    const targetItem = list.find((item) => String(item.id) === String(id));

    if (targetItem) return targetItem[prop];
    return "Ошибка";
  };

  return (
    <div className="citizens">
      <div className="buttons">
        <button onClick={onEdit} disabled={!selectedId}>
          Редактировать
        </button>
        <button onClick={onDel} disabled={!selectedId || citizenForEdit.id}>
          Удалить
        </button>
      </div>
      <table className="citizens-list">
        <thead>
          <tr className="table-header">
            <th>id</th>
            <th>ИНН</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Номер документы</th>
          </tr>
        </thead>
        <tbody>
          {citizens.map((item) => (
            <tr
              key={item.id}
              onClick={() => onSelect(item.id)}
              className={cn("list-element", {
                "list-element--active": item.id === selectedId,
              })}
            >
              <td>{item.id}</td>
              <td>{getProperty(item.personId, persons, "inn")}</td>
              <td>{item.lastName}</td>
              <td>{item.secondName}</td>
              <td>{item.lastName}</td>
              <td>{getProperty(item.documentId, documents, "seriya")}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <form onSubmit={onAdd} className="add-form">
        <input
          value={firstName}
          onChange={handleChange}
          name="firstName"
          placeholder="Имя"
        />
        <input
          value={secondName}
          onChange={handleChange}
          name="secondName"
          placeholder="Отчество"
        />
        <input
          value={lastName}
          onChange={handleChange}
          name="lastName"
          placeholder="Фамилия"
        />
        <select name="personId" value={personId} onChange={handleChange}>
          <option value="" disabled></option>
          {persons.map((item) => (
            <option key={item.id} value={item.id}>
              {item.inn}
            </option>
          ))}
        </select>
        <select name="documentId" value={documentId} onChange={handleChange}>
          <option value="" disabled></option>
          {documents.map((item) => (
            <option key={item.id} value={item.id}>
              {item.seriya}
            </option>
          ))}
        </select>
        <button type="submit">
          {citizenForEdit ? "Сохранить" : "Добавить"}
        </button>
      </form>
    </div>
  );
};

export default memo(Sitizens);
