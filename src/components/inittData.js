export default {
  persons: [
    {
      id: 1,
      shifer: "12x21w1s21",
      inn: 12321321,
      type: "Физ. лицо",
      date: "14.04.2020",
    },
    {
      id: 2,
      shifer: "wwwwwwww",
      inn: 3232323,
      type: "Физ. лицо",
      date: "14.04.2020",
    },
    {
      id: 3,
      shifer: "cccccccccc",
      inn: 123232,
      type: "Юр. лицо",
      date: "14.04.2020",
    },
  ],
  documents: [
    {
      id: 1,
      seriya: 12321313,
      organ: 'Организация',
      name: "Паспорт",
      date: "14.04.2020",
    },
    {
      id: 2,
      seriya: 3213123,
      organ: 'Паспортный',
      name: "Паспорт",
      date: "14.04.2020",
    }, {
      id: 3,
      seriya: 222222,
      organ: 'Организация',
      name: "Загран",
      date: "14.04.2020",
    },
  ],
  citizens: [
    {
      id: 1,
      personId: 1,
      documentId: 1,
      firstName: 'Василий',
      secondName: 'Петрович',
      lastName: "Курочкин",
    },
    {
      id: 2,
      personId: 2,
      documentId: 2,
      firstName: 'Иван',
      secondName: 'Иванович',
      lastName: "Иванов",
    }, {
      id: 3,
      personId: 3,
      documentId: 3,
      firstName: 'Дмитрий',
      secondName: 'Дмитричевич',
      lastName: "Дмитренко",
    },
  ],
}