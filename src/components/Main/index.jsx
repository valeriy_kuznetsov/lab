import React, { memo, useState, useCallback } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import initData from "../inittData";
import Persons from "../Persons";
import Documents from "../Documents";
import Citizens from "../Citizens";

import "./index.css";

const Main = () => {
  const [persons, setPersons] = useState(initData.persons);
  const [documents, setDocuments] = useState(initData.documents);
  const [citizens, setCitizens] = useState(initData.citizens);

  const addPerson = useCallback(
    (data) => {
      let newPersons;
      if (data.id) {
        newPersons = persons.map((item) => {
          if (item.id === data.id) {
            return data;
          }
          return item;
        });
      } else {
        newPersons = [
          ...persons,
          {
            id: Math.random().toString(32).slice(2),
            shifer: Math.random().toString(32).slice(2),
            date: new Date().toLocaleDateString(),
            ...data,
          },
        ];
      }

      setPersons(newPersons);
    },
    [persons]
  );

  const addDocument = useCallback(
    (data) => {
      let newDocuments;
      if (data.id) {
        newDocuments = documents.map((item) => {
          if (item.id === data.id) {
            return data;
          }
          return item;
        });
      } else {
        newDocuments = [
          ...documents,
          {
            id: Math.random().toString(32).slice(2),
            date: new Date().toLocaleDateString(),
            ...data,
          },
        ];
      }

      setDocuments(newDocuments);
    },
    [documents]
  );

  const addCitizen = useCallback(
    (data) => {
      let newCitizens;
      if (data.id) {
        newCitizens = citizens.map((item) => {
          if (item.id === data.id) {
            return data;
          }
          return item;
        });
      } else {
        newCitizens = [
          ...citizens,
          {
            id: Math.random().toString(32).slice(2),
            date: new Date().toLocaleDateString(),
            ...data,
          },
        ];
      }

      setCitizens(newCitizens);
    },
    [citizens]
  );

  const deletePerson = useCallback(
    (id) => {
      const filteredPersons = persons.filter((item) => item.id !== id);
      setPersons(filteredPersons);
    },
    [persons]
  );

  const deleteDocument = useCallback(
    (id) => {
      const filteredDocuments = documents.filter((item) => item.id !== id);
      setDocuments(filteredDocuments);
    },
    [documents]
  );

  const deleteCitizen = useCallback(
    (id) => {
      const filteredCitizens = citizens.filter((item) => item.id !== id);
      setCitizens(filteredCitizens);
    },
    [citizens]
  );

  return (
    <div className="main-contet">
      <Tabs>
        <TabList>
          <Tab>Citizens</Tab>
          <Tab>Persons</Tab>
          <Tab>Document</Tab>
        </TabList>

        <TabPanel>
          <Citizens
            addCitizen={addCitizen}
            deleteCitizen={deleteCitizen}
            documents={documents || []}
            persons={persons || []}
            citizens={citizens || []}
          />
        </TabPanel>
        <TabPanel>
          <Persons
            addPerson={addPerson}
            deletePerson={deletePerson}
            persons={persons || []}
          />
        </TabPanel>
        <TabPanel>
          <Documents
            documents={documents || []}
            addDocument={addDocument}
            deleteDocument={deleteDocument}
          />
        </TabPanel>
      </Tabs>
    </div>
  );
};

export default memo(Main);
