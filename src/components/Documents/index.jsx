import React, { memo, useState, useCallback } from "react";
import cn from "classnames";

import "./index.css";

const Documents = ({ addDocument, deleteDocument, documents }) => {
  const [documentForEdit, setDocumentForEdit] = useState({});
  const [selectedId, setSelectedId] = useState(null);

  const [seriya, setSeriya] = useState("");
  const [organ, setOrgan] = useState("");
  const [name, setName] = useState("");

  const onEdit = () => {
    const targetDocument = documents.find((item) => item.id === selectedId);

    setName(targetDocument.name);
    setSeriya(targetDocument.seriya);
    setOrgan(targetDocument.organ);
    setDocumentForEdit(targetDocument);
  };

  const onDel = useCallback(() => {
    deleteDocument(selectedId);
    setSelectedId(null);
  }, [deleteDocument, selectedId]);

  const handleChange = ({ target }) => {
    const { name, value } = target;

    if (name === "name") setName(value.trim());
    if (name === "organ") setOrgan(value);
    if (name === "seriya") setSeriya(value);
  };

  const onSelect = useCallback(
    (id) => {
      setSelectedId(selectedId && selectedId === id ? null : id);
      setDocumentForEdit({});
      setName("");
      setOrgan("");
      setSeriya("");
    },
    [selectedId]
  );

  const validForm = useCallback(() => {
    if (seriya.trim() === "") return false;
    if (name.trim() === "") return false;
    if (organ.trim() === "") return false;
    return true;
  }, [name, organ, seriya]);

  const onAdd = useCallback(
    (e) => {
      e.preventDefault();

      if (validForm()) {
        const data = {
          ...documentForEdit,
          organ,
          seriya,
          name,
        };
        setName("");
        setOrgan("");
        setSeriya("");
        setDocumentForEdit({});
        addDocument(data);
      }
    },
    [addDocument, documentForEdit, name, organ, seriya, validForm]
  );

  return (
    <div className="document">
      <div className="buttons">
        <button onClick={onEdit} disabled={!selectedId}>
          Редактировать
        </button>
        <button onClick={onDel} disabled={!selectedId || documentForEdit.id}>
          Удалить
        </button>
      </div>
      <table className="document-list">
        <thead>
          <tr className="table-header">
            <th>id</th>
            <th>Серия</th>
            <th>Название</th>
            <th>Организация</th>
            <th>Дата выдачи</th>
          </tr>
        </thead>
        <tbody>
          {documents.map((item) => (
            <tr
              key={item.id}
              onClick={() => onSelect(item.id)}
              className={cn("list-element", {
                "list-element--active": item.id === selectedId,
              })}
            >
              <td>{item.id}</td>
              <td>{item.seriya}</td>
              <td>{item.name}</td>
              <td>{item.organ}</td>
              <td>{item.date}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <form onSubmit={onAdd} className="add-form">
        <input
          value={name}
          onChange={handleChange}
          name="name"
          placeholder="Документ"
        />
        <input
          value={seriya}
          onChange={handleChange}
          name="seriya"
          placeholder="Серия"
        />
        <input
          value={organ}
          onChange={handleChange}
          name="organ"
          placeholder="Выдавший орган"
        />
        <button type="submit">
          {documentForEdit ? "Сохранить" : "Добавить"}
        </button>
      </form>
    </div>
  );
};

export default memo(Documents);
